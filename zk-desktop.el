;;; zk-desktop.el --- playful, "desktop" ordering zettels     -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Alec Kornblum

;; Author: Alec M. Kornblum <alec.kornblum@gmail.com>
;; Version: 1.0
;; Package-Requires: ((emacs "28.1") (dash "2.19.1"))
;; Keywords: windows
;; URL: TBA

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; This file is not part of GNU Emacs

;;; Commentary:

;; This package provides functionality to "play around" with
;; zettelkasten-style notes, as though using your hands to rearrange
;; notecards on a desktop.

;;; Code:

(require 'cl-lib)
(require 'dash)

;;;; User Variables

(defvar zk-desktop-zettelkasten-directory nil
  "Configure this variable to be the directory in which your
permanent notes are stored.

NOTE: does not support subdirectories.")
(defvar zk-desktop-max-rows 4
  "Maximum number of rows before adding a column in zk-desktop")
(defvar zk-desktop-max-columns 3
  "Maximum number of rows before adding columns is halted and an
error is signaled.")
(defvar zk-desktop-prefer-rows-or-columns 'rows
  "Indicates which to add first until hitting the max, 'rows or
'cols")
(defvar zk-desktop-activate-view-mode t
  "User variable to decide whether to activate `view-mode' upon
displaying a new zettel.")
(defvar zk-desktop-persistent-file-lists-directory user-emacs-directory
  "The directory in which to save your persistent file lists.
Defaults to `user-emacs-directory' because I couldn't think of a
better default!")
(defvar zk-desktop-user-configuration nil
  "This alist allows for easy configuration of `zk-desktop' via
completion. Each item should be a list consisting of a short
string description of the usage for the configuration, followed
by the max rows and columns.

Suggested item formatting: (\"short name X rows Y columns\" X Y)")

;;;; Utilities

(defun zk-desktop--alist-complete (alist prompt &optional callback history predicate require-match initial-input default inherit-input-method)
  "This is a utility function to allow for easier use of
`completing-read' for alists. It allows any item in an alist to
be matched on. It also provides optional CALLBACK to limit the
returned data to exactly what you need. 

Example usage of `alist-complete':

(setq alec-text-sizes '((alec-len \"Lenovo, no monitor attached\" 132)
                        (len-monitor-upstairs \"Upstairs, large monitor, higher DPI\" 201)
                        (5CG5336PFS \"Work computer\" 81)
                        (DPKCND2012MBX \"Work tablet\" 96)
                        (alec-fw \"Framework, no monitor\" 75)
                        (monitor-upstairs \"Upstairs large monitor\" 63)))

(defun alec--select-text-size ()
    (interactive)
    (let* ((new-size (alist-complete alec-text-sizes \"Choose a text size: \" #'cl-third)))
      (set-face-attribute 'default (selected-frame) :height new-size)))

Very concise! Perhaps, though, you are interested in a customized
message for each possible selection. For longer associated
values, use `seq-let' for the most concise syntax:

(setq alec-text-sizes '((alec-len \"Lenovo, no monitor attached\" 132 \"304-555-1234\" \"1234 Gerald Lane\")
                          (len-monitor-upstairs \"Upstairs, large monitor, higher DPI\" 201 \"305-666-6789\" \"1800 Cash Now Blvd\")
                          (5CG5336PFS \"Work computer\" 81 \"306-555-1234\" \"8675 Three Oh Nine Drive\")
                          (DPKCND2012MBX \"Work tablet\" 96 \"307-555-5678\" \"100 Houston Street\")
                          (alec-fw \"Framework, no monitor\" 75 \"308-555-1234\" \"101 Engineering Towne Park Drive\")
                          (monitor-upstairs \"Upstairs large monitor\" 63 \"304-555-1234\" \"64 Sidewinder Road\")))

(let ((selection (alist-complete alec-text-sizes \"Choose a text size: \")))
  (seq-let (id msg size phone address) selection
    (set-face-attribute 'default (selected-frame) :height size) ;; allegedly something useful!
    (message \"%s for size %s; call %s for questions or mail complaints to %s.\" msg size phone address)))"
  (let* ((completions (cl-loop for item in alist
                               collect (s-join "  " (cl-loop for atom in item
                                                             collect (prin1-to-string atom t)))))
         (completions-alist (cl-loop for i from 0
                                     for completion in completions
                                     collect (list completion (elt alist i))))
         (selection (car (alist-get (completing-read prompt completions-alist
                                                     predicate require-match
                                                     initial-input history
                                                     default inherit-input-method)
                                    completions-alist nil nil #'string-equal))))
    (funcall (or callback #'identity) selection)))

(defun zk-desktop--directory-files-only (directory &optional absolute)
  "Helper to give me all files in DIRECTORY that aren't \".\" or
\"..\""
  (let* ((notes (directory-files directory absolute))
	 (base (if absolute (expand-file-name directory) ""))
	 (notes (delete (concat base ".") notes))
	 (notes (delete (concat base "..") notes)))
    notes))

(defun zk-desktop--zettelkasten-files ()
  "Uses `zk-desktop-zettelkasten-directory' to return a list of all
files in that directory"
  (zk-desktop--directory-files-only zk-desktop-zettelkasten-directory))

(defun zk-desktop--zettel-truename (file)
  (let ((zettel (file-name-concat zk-desktop-zettelkasten-directory file)))
    (if (file-exists-p zettel)
	(file-truename zettel)
      (error "Error in configuration, %s is not a real file. Check your
`user-init-file'." zettel))))

(defun zk-desktop--zettel-truename-from-id (id)
  (if (bufferp id)
      id
    (thread-last (directory-files zk-desktop-zettelkasten-directory t id)
		 (cl-first))))

(defun zk-desktop--zettel-buffer (id)
  (thread-last id
	       zk-desktop--zettel-truename-from-id
	       find-file-noselect))

;;;; File Tracking

;;;;; file list

(defvar zk-desktop--ids nil
  "When non-nil, this variable stores the order of the files being
displayed and rearranged.")

(defun zk-desktop--ids ()
  "Returns a newline-separated string of the file names from
`zk-desktop--ids'. This is used primarily for debugging and
manual testing."
  (cl-loop for file in zk-desktop--ids
	   concat (format "%s\n" file))
  ;; (let (nice-files)
  ;;   (dolist (file zk-desktop--ids)
  ;;     (setq nice-files (concat nice-files (format "%s\n" (buffer-name buffer)))))
  ;;   nice-files)
  )

(defun zk-desktop-current-list-length ()
  "Displays message showing how many files are currently tracked in
`zk-desktop--files.'"
  (interactive)
  (message "%s" (length zk-desktop--ids)))

(defvar zk-desktop--start-methods
  `(("from scratch" ,#'zk-desktop-start-from-scratch)
    ("from saved" ,#'zk-desktop-switch-to-file-list))
  "Ways `zk-desktop' can start")

(defun zk-desktop-start (method)
  "Starts the desktop session."
  (interactive (list (zk-desktop--alist-complete zk-desktop--start-methods "Start: " #'cadr)))
  (let* ((offer-save (and zk-desktop--ids
			  (not (equal method #'zk-desktop-switch-to-file-list))))) ; TODO: all checks go here
    ;; possibly save
    (when (and offer-save
	       (y-or-n-p "Already working with files. Do you want to save your session first?"))
      (if zk-desktop-current-files-list
	  (zk-desktop-save-current-file-list)
	(zk-desktop-save-file-list)))
    (zk-desktop--ensure-swap-advices)
    (call-interactively method)))

;;;###autoload
(defalias #'zk-desktop #'zk-desktop-start)

(defun zk-desktop-start-from-scratch ()
  "Starts a `zk-desktop' session from scratch, tracking the
currently displayed files, ignoring BLANKS."
  (interactive)
  (let ((starting-files (when (y-or-n-p "Start with currently displayed files? ")
			  (zk-desktop--get-displayed-files-no-blanks))))
    (when starting-files
      (mapc #'zk-desktop--activate-view-mode
	    (mapcar #'window-buffer (zk-desktop--get-window-list-no-blanks))))
    (setq zk-desktop--ids starting-files
	  zk-desktop-current-files-list nil)))

(defun zk-desktop-start-from-saved ()
  "Starts a `zk-desktop' from a saved list."
  (interactive)
  (zk-desktop-start (car (alist-get "from saved" zk-desktop--start-methods
				    nil nil #'string-equal))))

(defun zk-desktop-finish ()
  "Deletes saved files from `zk-desktop--ids'. Issues a
warning."
  (interactive)
  (when (and zk-desktop-current-files-list (y-or-n-p "Save current working list?"))
    (zk-desktop-save-current-file-list))
  (setq zk-desktop--ids nil
	zk-desktop-current-files-list nil)
  (zk-desktop--remove-swap-advices))


;;;;; Persistence of file lists

(defvar zk-desktop-current-files-list nil
  "Indicates the file list being edited currently. This only
applies when a file list has been persisted!")

(defun zk-desktop--fix-name (name)
  "Fixes a name from a prompt. It removes illegal characters,
changes spaces and underscores to hyphens, deletes duplicate hyphens,
and downcases the final name."
  (setq name (s-replace-regexp (rx (any "[][{}!@#$%^&*()=+'\"?,.\|;:~`‘’“”/]*"))
			       "" name)
	name (s-replace-regexp (rx (any space
					"_"))
			       "-" name)
	name (s-replace-regexp (rx (one-or-more "-")) "-" name)
	name (downcase name)))

(defun zk-desktop--read-new-file-list-name (&optional renaming)
  "Reads a string to name a saved file list. Originally extracted
from `zk-desktop-save-file-list' for reuse in
`zk-desktop-rename-file-list'.

Appends `zk-desktop-current-files-list' name to the end for
reference (for use when RENAMING a file list)."
  (let* ((prompt (format "Name your file list: %s"
			 (if renaming
			     (format "(old: %s) "
				     (file-name-nondirectory zk-desktop-current-files-list))
			   "")))
	 (name (read-string prompt)))
    (setq name (zk-desktop--fix-name name))))

(defun zk-desktop-save-file-list (name)
  "Saves the file list to a corresponding file in the
`zk-desktop-persistent-file-lists-directory'."
  (interactive (list (zk-desktop--read-new-file-list-name)))
  (let ((persistent-list-file (concat zk-desktop-persistent-file-lists-directory name)))
    (with-temp-file persistent-list-file
      (print zk-desktop--ids (current-buffer)))
    (setq zk-desktop-current-files-list persistent-list-file)))

(defalias #'zk-desktop-save-file-list-as #'zk-desktop-save-file-list)
(defalias #'zk-desktop-save-copy-of-file-list #'zk-desktop-save-file-list)

(defun zk-desktop--read-file-list (&optional new-allowed overwrite-prompt)
  "Uses `completing-read' to select a saved file list."
  (unless overwrite-prompt
    (setq overwrite-prompt "Select a file list: "))
  (let* ((new-allowed (not new-allowed))
	 (selection (read-file-name overwrite-prompt
				    zk-desktop-persistent-file-lists-directory
				    nil new-allowed)))
    selection))

(defun zk-desktop--get-file-list (&optional file-list)
  "Returns the contents of a saved file list."
  (unless file-list
    (setq file-list (zk-desktop--read-file-list)))
  (with-temp-buffer
    (insert-file-contents file-list)
    (goto-char (point-min))
    (read (current-buffer))))

(defalias #'zk-desktop--get-file-list-contents #'zk-desktop--get-file-list)
(defalias #'zk-desktop--read-file-list-contents #'zk-desktop--get-file-list)

(defun zk-desktop-show-file-list ()
  "Display a message with the contents of a saved file list!

WARNING: these lists can be long, the message might not be so
useful!"
  (interactive)
  (pp (zk-desktop--get-file-list) t))

(defun zk-desktop-rename-file-list (file-list)
  "Renames a file list using minibuffer prompts."
  (interactive (list (zk-desktop--read-file-list)))
  (let* ((new-name (zk-desktop--read-new-file-list-name t))
	 (new-file-list (concat zk-desktop-persistent-file-lists-directory new-name)))
    (rename-file file-list new-file-list)
    new-file-list))

(defun zk-desktop-rename-current-file-list ()
  "Choose a new name for the currently used file list."
  (interactive)
  (setq zk-desktop-current-files-list (zk-desktop-rename-file-list zk-desktop-current-files-list)))

(defun zk-desktop-save-current-file-list ()
  "Saves the currently active file list, using variable
`zk-desktop-current-files-list'."
  (interactive)
  (if zk-desktop-current-files-list
      (progn
	(zk-desktop-save-file-list (file-name-nondirectory zk-desktop-current-files-list))
	(message "Saved file list titled: %s" (file-name-nondirectory zk-desktop-current-files-list)))
    (call-interactively #'zk-desktop-save-file-list)
    (message "Saved file list titled: %s" (file-name-nondirectory zk-desktop-current-files-list))))

(defun zk-desktop-delete-file-list ()
  "Deletes a saved file list."
  (interactive)
  (let* ((file-list (zk-desktop--read-file-list nil "Delete which saved file list? "))
	 (file-list-name (file-name-nondirectory file-list)))
    (when (y-or-n-p (format "Are you sure you want to delete %s list? " file-list-name))
      (delete-file file-list))))

(defun zk-desktop-switch-to-file-list ()
  "Changes your `zk-desktop-current-files-list' to a different
persisted file list. Offers to save the
`zk-desktop-current-files-list' before switching.

WARNING: this will shift your file list to be displaying only
the first X zettels, where X is the number of cells (* rows
columns) of your current grid of windows. In other words, you'll
lose your place in your current list."
  (interactive)
  (when (and zk-desktop--ids zk-desktop-current-files-list)
    (let ((save-first? (y-or-n-p "Do you want to save your current file list before switching? ")))
      (when save-first?
	(zk-desktop-save-current-file-list))))
  (let* ((from? (if (and zk-desktop--ids zk-desktop-current-files-list)
		    (format "from %s " (file-name-nondirectory zk-desktop-current-files-list))
		  ""))
	 (new (zk-desktop--read-file-list nil
					  (format "Switch %sto new file list: " from?))))
    (setq zk-desktop-current-files-list new)
    (setq zk-desktop--ids
	  (zk-desktop--get-file-list zk-desktop-current-files-list))
    (let ((new-files (zk-desktop--pad-with-blanks zk-desktop--ids
						  (length (zk-desktop--get-window-list)))))
      (zk-desktop--redisplay-files new-files)
      (zk-desktop--ensure-swap-advices))))

;;;; Display Buffer Override

(defun zk-desktop--display-buffer (buffer)
  "Defines `display-buffer' behavior for `zk-desktop', entirely
overriding functionality except for the use of
`zk-desktop--display-buffer-action'."
  (let ((display-buffer-overriding-action (cons #'zk-desktop--display-buffer-action nil))
	(display-buffer-alist '(nil))
	(display-buffer-base-action '(nil))
	(display-buffer-fallback-action '(nil)))
    (let ((window (display-buffer buffer)))
      (when window
	(select-window window)))))

(defun zk-desktop--activate-view-mode (buffer)
  "Activates `view-mode' in BUFFER"
  (with-current-buffer buffer
    (view-mode 1)))

(defun zk-desktop--display-buffer-action (buffer buffer-alist)
  "Display buffer action function for `zk-desktop'. Places files
in a *BLANK* buffer preferentially, otherwise creating a new row
or column DWIM style based on `zk-desktop-max-rows',
`zk-desktop-max-columns', and
`zk-desktop-prefer-rows-or-columns'. Prints a message if it
cannot display the buffer."
  (if-let ((blank-window (zk-desktop--get-blank-buffer-window 0)))
      (progn
	(set-window-buffer blank-window buffer)
	(when zk-desktop-activate-view-mode
	  (zk-desktop--activate-view-mode buffer))
	blank-window)
    (if-let ((new (zk-desktop--decide-add-row-or-column)))
	(let (window)
	  (cond ((equal new 'rows)
		 (setq window
		       (split-window (car (last (zk-desktop--get-window-list))) nil 'down)))
		((equal new 'columns)
		 (progn
		   (setq window (split-window (funcall (zk-desktop--column-split-decider)) nil 'right))
		   (let ((rows (zk-desktop--count-rows)))
		     (when (> rows 1)
		       (dotimes (_ (1- rows))
			 (let ((needs-blank (split-window window nil 'down)))
			   (set-window-buffer needs-blank (zk-desktop--blank-buffer)))))))))
	  (balance-windows)
	  (window--display-buffer buffer window 'reuse buffer-alist)
	  (when zk-desktop-activate-view-mode
	    (zk-desktop--activate-view-mode buffer))
	  window)
      (message "Could not display %s" buffer))))


;;;; Redisplay files (including shifting and adding rows or columns manually)

;;;;; Adding columns and rows

(defun zk-desktop-view-more-notes ()
  "Adds notes to the current view.

WARNING: This function likely only works with rows-first
preference as of [2023-02-25 Sat 14:57]."
  (interactive)
  (let* ((decision (zk-desktop--decide-add-row-or-column))
	 (how-many? (if (equal decision zk-desktop-prefer-rows-or-columns)
			1
		      (symbol-value (intern (concat "zk-desktop-max-" (symbol-name zk-desktop-prefer-rows-or-columns))))))
	 (displayed-ids (mapcar #'(lambda (window) (thread-first (zk-desktop--window-buffer-file window)
								 (seq-subseq 0 15)))
				(zk-desktop--get-window-list-no-blanks)))

	 (len-il (length displayed-ids))
	 (il-location (zk-desktop--find-subseq zk-desktop--ids displayed-ids))
	 (next-file-index (+ il-location len-il))
	 (end (+ next-file-index how-many? -1))
	 (next-id-or-ids (if (equal how-many? 1)
			     (elt zk-desktop--ids next-file-index)
			   (cl-loop for i from next-file-index to end
				    if (elt zk-desktop--ids i)
				    collect (elt zk-desktop--ids i)
				    else
				    collect (zk-desktop--blank-buffer)))))
    (cond ((stringp next-id-or-ids) (zk-desktop--display-buffer (find-file-noselect (zk-desktop--zettel-truename-from-id next-id-or-ids))))
	  ((sequencep (dolist (id next-id-or-ids)
			(if (bufferp id)
			    (zk-desktop--display-buffer id)
			  (zk-desktop--display-buffer (find-file-noselect (zk-desktop--zettel-truename-from-id id))))
			)))
	  ((null next-id-or-ids) (user-error "No more files to display")))
    (let ((map (make-sparse-keymap)))
      (define-key map (kbd "v") #'zk-desktop-view-more-notes)
      (set-transient-map map t))))



(defun zk-desktop-add-column ()
  "Uses the `zk-desktop' windowing style to add a column of
windows."
  (interactive)
  (let ((columns-count (zk-desktop--count-columns)))
    (if (>= columns-count zk-desktop-max-columns)
	(user-error "Reached max columns!")
      (let ((goal-columns (1+ columns-count))
	    (adder-function (cond (zk-desktop--ids #'zk-desktop-view-more-notes)
				  (t #'(lambda () (zk-desktop--display-buffer (zk-desktop--blank-buffer)))))))
	(while (< (zk-desktop--count-columns) goal-columns)
	  (funcall adder-function))))))

(defun zk-desktop-add-row ()
  "Uses the `zk-desktop' windowing style to add a row of
windows."
  (interactive)
  (let ((rows-count (zk-desktop--count-rows)))
    (if (>= rows-count zk-desktop-max-rows)
	(user-error "Reached max rows!")
      (let ((goal-rows (1+ rows-count))
	    (adder-function (cond (zk-desktop--ids #'zk-desktop-view-more-notes)
				  (t #'(lambda () (zk-desktop--display-buffer (zk-desktop--blank-buffer)))))))
	(while (< (zk-desktop--count-rows) goal-rows)
	  (funcall adder-function))))))

;;;;; windowmove swaps

(defun zk-desktop--swap-files (swap-this with-this)
  (let* ((fl (zk-desktop--get-displayed-files-no-blanks))
	 (start (zk-desktop--find-subseq zk-desktop--ids fl))
	 (swap-this-index (zk-desktop--get-window-index swap-this))
	 (with-this-index (zk-desktop--get-window-index with-this))
	 (swap-this-file (zk-desktop--window-buffer-file swap-this))
	 (with-this-file (zk-desktop--window-buffer-file with-this))
	 (swap-this-buffer-index (+ start swap-this-index))
	 (with-this-buffer-index (+ start with-this-index)))
    (setf (elt zk-desktop--ids swap-this-buffer-index) with-this-file
	  (elt zk-desktop--ids with-this-buffer-index) swap-this-file)))

(defun zk-desktop--swap-up ()
  (let* ((bottom-window (selected-window))
	 (top-window (window-in-direction 'up nil windmove-allow-all-windows
					  nil windmove-wrap-around 'nomini)))
    (zk-desktop--swap-files bottom-window top-window)))

(defun zk-desktop-swap-up ()
  (interactive)
  (zk-desktop--swap-up)
  (windmove-swap-states-up))

(defun zk-desktop--swap-down ()
  (let* ((top-window (selected-window))
	 (bottom-window (window-in-direction 'down nil windmove-allow-all-windows
					     nil windmove-wrap-around 'nomini)))
    (zk-desktop--swap-files top-window bottom-window)))

(defun zk-desktop-swap-down ()
  (interactive)
  (zk-desktop--swap-down)
  (windmove-swap-states-down))

(defun zk-desktop--swap-right ()
  (let* ((left-window (selected-window))
	 (right-window (window-in-direction 'right nil windmove-allow-all-windows
					    nil windmove-wrap-around 'nomini)))
    (zk-desktop--swap-files left-window right-window)))

(defun zk-desktop-swap-right ()
  (interactive)
  (zk-desktop--swap-right)
  (windmove-swap-states-right))

(defun zk-desktop--swap-left ()
  (let* ((right-window (selected-window))
	 (left-window (window-in-direction 'left nil windmove-allow-all-windows
					   nil windmove-wrap-around 'nomini)))
    (zk-desktop--swap-files right-window left-window)))

(defun zk-desktop-swap-left ()
  (interactive)
  (zk-desktop--swap-left)
  (windmove-swap-states-left))

;; (setq permanent-file-list (copy-tree zk-desktop--ids))
;; (setq zk-desktop--ids permanent-file-list)

(defun zk-desktop--add-swap-advices ()
  (advice-add #'windmove-swap-states-up :before #'zk-desktop--swap-up)
  (advice-add #'windmove-swap-states-down :before #'zk-desktop--swap-down)
  (advice-add #'windmove-swap-states-right :before #'zk-desktop--swap-right)
  (advice-add #'windmove-swap-states-left :before #'zk-desktop--swap-left))

(defun zk-desktop--remove-swap-advices ()
  (advice-remove #'windmove-swap-states-up #'zk-desktop--swap-up)
  (advice-remove #'windmove-swap-states-down #'zk-desktop--swap-down)
  (advice-remove #'windmove-swap-states-right #'zk-desktop--swap-right)
  (advice-remove #'windmove-swap-states-left #'zk-desktop--swap-left))

(defun zk-desktop--ensure-swap-advices ()
  (unless (advice-member-p #'zk-desktop--swap-up #'windmove-swap-states-up)
    (advice-add #'windmove-swap-states-up :before #'zk-desktop--swap-up))
  (unless (advice-member-p #'zk-desktop--swap-down #'windmove-swap-states-down)
    (advice-add #'windmove-swap-states-down :before #'zk-desktop--swap-down))
  (unless (advice-member-p #'zk-desktop--swap-right #'windmove-swap-states-right)
    (advice-add #'windmove-swap-states-right :before #'zk-desktop--swap-right))
  (unless (advice-member-p #'zk-desktop--swap-left #'windmove-swap-states-left)
    (advice-add #'windmove-swap-states-left :before #'zk-desktop--swap-left)))

;;;; Completing read for move-note

(defun zk-desktop--move-note (file-list from-index to-index)
  "Returns a list with FILE-LIST item in FROM-INDEX moved to
TO-INDEX."
  (when (< from-index 0)
    (user-error "Must move a note with a positive index"))
  (let* ((move-this (elt file-list from-index))
	 (removed-list (seq-remove #'(lambda (elt) (equal elt move-this)) file-list))
	 (beginning (cond ((< to-index 0) nil)
			  (t (seq-take removed-list to-index))))
	 (ending (condition-case err
		     (cond ((< to-index 0) removed-list)
			   (t (seq-subseq removed-list to-index)))
		   (error nil))))
    (append beginning (list move-this) ending)))



(defun zk-desktop--current-zettels-indexed-alist ()
  "Returns the current zettels (`zk-desktop--ids') as an alist
with the zettel-list index as the first element."
  (interactive)
  (cl-loop for i from 0
	   for id in zk-desktop--ids
	   collect (list i (zk-desktop--nice-note-selection id))))

(defun zk-desktop-move-note ()
  "Choose a note to move from it's current position in the
tracked ids list, to a position just before the id selected by a
`completing-read' from `zk-desktop--ids'."
  (interactive)
  (let* ((start-of-seq (zk-desktop--find-subseq zk-desktop--ids (zk-desktop--window-id-list)))
	 (move-this-zettel (zk-desktop--read-zettel nil #'identity))
	 (move-this-file-index (car move-this-zettel))
	 (move-this-file-title (cl-third move-this-zettel))
	 (files-alist (append (list '(-1 "beginning"))
			      (zk-desktop--current-zettels-indexed-alist)
			      (list `(,(length zk-desktop--ids) "end"))))
	 (prompt (format "Move %s before the selected note: " move-this-file-title))
	 (to-this-index (let ((vertico-sort-function nil))
		          (zk-desktop--alist-complete files-alist prompt #'car
						      ))))
    (setq zk-desktop--ids (zk-desktop--move-note zk-desktop--ids
						 move-this-file-index
						 to-this-index))
    (zk-desktop--redisplay-files (seq-subseq zk-desktop--ids start-of-seq))))

(defun zk-desktop-move-this-note ()
  "Move the note where `point' is from it's current position in the
tracked files list, to a position just before the selected
file; uses `completing-read' to select from `zk-desktop--ids'."
  (interactive)
  (let* ((start-of-seq (zk-desktop--find-subseq zk-desktop--ids (zk-desktop--window-id-list)))
	 (current-window-index (zk-desktop--get-window-index))
	 (move-this-file-index (+ start-of-seq current-window-index))
	 (files-alist (append (list '(-1 "beginning"))
			      (zk-desktop--current-zettels-indexed-alist)
			      (list `(,(length zk-desktop--ids) "end"))))
	 (prompt (format "Move %s before the selected note: " (file-name-nondirectory (buffer-file-name (current-buffer)))))
	 (to-this-index (let ((vertico-sort-function nil))
		          (zk-desktop--alist-complete files-alist prompt #'car
						      ))))
    (setq zk-desktop--ids (zk-desktop--move-note zk-desktop--ids
						 move-this-file-index
						 to-this-index))
    (zk-desktop--redisplay-files (seq-subseq zk-desktop--ids start-of-seq))))

;;;; Stranded notes 

(defun zk-desktop--stranded-notes (id-list)
  "Returns a list of the stranded notes filenames."
  (let* ((notes
	  (thread-last
	    (zk-desktop--directory-files-only zk-desktop-zettelkasten-directory)
	    (seq-remove #'(lambda (elt) (s-matches-p (rx "__"
							 (zero-or-more any)
							 "fleeting"
							 (zero-or-more any)
							 ".org")
						     elt)))
	    (mapcar #'(lambda (elt) (seq-subseq elt 0 15)))
	    (seq-remove #'(lambda (elt) (seq-contains-p id-list elt))))))
    notes))


(defun zk-desktop--nice-note-alist-from-id (id)
  (list id (zk-desktop--nice-note-selection id t)))


(defun zk-desktop-stranded-notes (&optional callback)
  "Shows a list of the stranded notes, with reference to the
currently tracked file list. Uses
`zk-desktop-zettelkasten-directory' files to compare and check
for stranded notes.

This is properly used when tracking your canonical zettelkasten
placement list.

The `completing-read' is incidental. Selecting an item has no
effect when using this function alone."
  (interactive)
  (unless callback
    (setq callback #'car))
  (if (not zk-desktop--ids)
      (user-error "Not tracking files in a saved list.")
    (let* ((stranded-notes (thread-last (zk-desktop--stranded-notes zk-desktop--ids)
					(mapcar #'zk-desktop--nice-note-alist-from-id))))
      (zk-desktop--alist-complete stranded-notes "Select a stranded note: " callback))))

(defun zk-desktop-find-stranded-note ()
  "Displays the selected stranded note in the current window."
  (interactive)
  (set-window-buffer nil (zk-desktop--zettel-buffer (zk-desktop-stranded-notes))))

;;;; Finding Random Notes

(defun zk-desktop--random-zettel (&optional zettels)
  (let* ((zettels (or zettels zk-desktop--ids))
	 (max (length zettels)))
    (elt zettels (random max))))

(defun zk-desktop-find-random-zettel ()
  (interactive)
  (set-window-buffer nil (zk-desktop--zettel-buffer (zk-desktop--random-zettel))))
(defalias #'zk-desktop-find-random-note #'zk-desktop-find-random-zettel)

(defun zk-desktop-find-random-stranded-zettel ()
  (interactive)
  (set-window-buffer nil
		     (thread-last (zk-desktop--stranded-notes zk-desktop--ids)
				  zk-desktop--random-zettel
				  zk-desktop--zettel-buffer)))
(defalias #'zk-desktop-find-random-stranded-note #'zk-desktop-find-random-stranded-zettel)

;;;; Place note

(defun zk-desktop--place-note (file-list note index)
  "Returns a list with FILE-LIST having NOTE at INDEX, other notes shifted accordingly."
  (append (seq-subseq file-list 0 index) (list note) (seq-subseq file-list index)))

(defun zk-desktop--nice-note-selection (id &optional omit-id)
  (let* ((file (zk-desktop--zettel-truename-from-id id))
	 (prompt (if omit-id "%s  %s" "%s  %s  %s"))
	 (args (list prompt))
	 (title (denote-retrieve-front-matter-title-value file 'org))
	 (keywords (denote-retrieve-front-matter-keywords-value file 'org)))
    (if omit-id
	(setf args (append args (list title keywords)))
      (setf args (append args (list id title keywords))))
    (apply #'format args)))

(defun zk-desktop--window-id-list ()
  (mapcar #'(lambda (window) (thread-first (zk-desktop--window-buffer-file window)
					   (seq-subseq 0 15)))
	  (zk-desktop--get-window-list-no-blanks)))

(defun zk-desktop-place-note (note-with-prompt)
  "Places NOTE in the tracked ID list.

Interactively, select a stranded note to place."
  (interactive (list (zk-desktop-stranded-notes #'identity)))
  (when zk-desktop--ids
    (let* ((note (cl-first note-with-prompt))
	   (prompt (cl-second note-with-prompt))
	   (files-alist (append (list '(-1 "beginning"))
				()
				(zk-desktop--current-zettels-indexed-alist)
				(list `(,(length zk-desktop--ids) "end"))))
	   (prompt (format "Place %s %s before the selected note: " note prompt))
	   (selection (let ((vertico-sort-function nil))
		        (zk-desktop--alist-complete files-alist prompt)))
	   (to-this-index (car selection))
           (start-of-seq (zk-desktop--find-subseq zk-desktop--ids (zk-desktop--window-id-list))))
      (setq zk-desktop--ids (zk-desktop--place-note zk-desktop--ids
			                            note
						    to-this-index))
      (zk-desktop--redisplay-files (seq-subseq zk-desktop--ids start-of-seq)))))

(defun zk-desktop-place-stranded-note ()
  "Integrates `zk-desktop-place-note' and
`zk-desktop-stranded-notes'."
  (interactive)
  (call-interactively #'zk-desktop-place-note))

;;;;; shifting

(defun zk-desktop--find-subseq (sequence subseq)
  "Iterates through a list, finding the index of the tracked file
that starts the currently displayed files."
  (cl-labels ((iter (times subseq-length compare-this to-this rest)
		(if (equal compare-this to-this)
		    times
		  (iter (1+ times) subseq-length (seq-subseq rest 0 subseq-length) to-this (cdr rest)))))
    (let* ((subseq-length (length subseq)))
      (condition-case err
	  (iter 0 subseq-length (seq-subseq sequence 0 subseq-length) subseq (cdr sequence))
	(error nil)))))

(defun zk-desktop--pad-with-blanks (mylist to-size &optional frontp)
  "Pads a list (MYLIST) with *BLANK* files so that the list is
TO-SIZE long.

Non-nil FRONTP means to pad the front of the list, leaving
default behavior to pad the end of the list."
  (let ((length-initial (length mylist))
	(filler (zk-desktop--blank-buffer))
	adder final)
    (dotimes (_ (- to-size length-initial))
      (setq adder (cons filler adder)))
    (cond (adder (if frontp
		     (setq final (append adder mylist))
		   (setq final (append mylist adder))))
	  (t (seq-take mylist to-size)))))

(defun zk-desktop--shift-few-files (window-list file-list vector)
  "Shifts files when there are fewer files than windows."
  (let ((first-non-blank-file-index (seq-position file-list
						  (seq-find
						   #'identity
						   file-list))))
    (condition-case err
	(cond ((> vector 0)
	       (let* ((num-front-blank-pads (1- first-non-blank-file-index))
		      
		      (front-blank-pad (cl-loop repeat num-front-blank-pads
						collect (zk-desktop--blank-buffer)))
		      (thru-notes (append front-blank-pad zk-desktop--ids))
		      (length-with-front-pad (length thru-notes)))
		 (if (< num-front-blank-pads 0)
		     (user-error "Reached end of file list, add more notes to continue shifting.")
		   (zk-desktop--pad-with-blanks thru-notes (length window-list)))))
	      ((< vector 0)
	       (let* ((num-front-blank-pads (1+ first-non-blank-file-index))
		      (front-blank-pad (cl-loop repeat num-front-blank-pads
						collect (zk-desktop--blank-buffer)))
		      (thru-notes (append front-blank-pad zk-desktop--ids))
		      (length-with-front-pad (length thru-notes)))
		 (if (> length-with-front-pad (length window-list))
		     (user-error "Reached end of file list, add more notes to continue shifting.")
		   (zk-desktop--pad-with-blanks thru-notes (length window-list))))))
      (user-error
       (setq zk-desktop--shifting-instructions (format "%s -- %s"
						       (error-message-string err)
						       zk-desktop--shifting-instructions))
       (set-transient-map zk-desktop--shifter)
       (-replace nil (zk-desktop--blank-buffer) file-list)))))

(defun zk-desktop--shift-many-files (window-list id-list vector)
  "Returns a shifted list of files, used when WINDOW-LIST is longer
than FILE-LIST. Shifting direction determined by VECTOR."
  (let* ((num-windows (length (zk-desktop--get-window-list)))
	 (displayed-zk-desktop-files (zk-desktop--get-displayed-files-no-blanks))
	 (displayed-zk-desktop-ids (mapcar #'(lambda (file) (seq-subseq file 0 15)) displayed-zk-desktop-files))
	 (num-displayed (length displayed-zk-desktop-files))
	 (il-location (zk-desktop--find-subseq zk-desktop--ids
					       displayed-zk-desktop-ids))
	 (last-displayed-location (+ il-location (1- num-displayed)))
	 (count-tracked-files (length zk-desktop--ids))
	 (start (+ vector il-location))
	 (end (+ start num-windows)))
    (condition-case err
	(cond ((and (equal il-location 0) ;; beginning of list
		    (< vector 0))
	       (user-error "Beginning of list! Cannot shift previous / upward."))
	      ((and (equal last-displayed-location (1- count-tracked-files)) ;; end of list
		    (> vector 0))
	       (user-error "End of list! Cannot shift next / down."))
	      ((> end count-tracked-files) ;; near end of list and blanks displayed, then try to shift
	       (setq zk-desktop--shifting-instructions
		     (format "%s -- %s" "Removed displayed blanks" zk-desktop--shifting-instructions))
	       (seq-subseq zk-desktop--ids (- num-windows)))
	      (t (seq-subseq zk-desktop--ids start end)))
      (user-error
       (setq zk-desktop--shifting-instructions (format "%s -- %s"
						       (error-message-string err)
						       zk-desktop--shifting-instructions))
       (set-transient-map zk-desktop--shifter)
       id-list))))

(defun zk-desktop--make-shifted-files-list (vector)
  "Produces the shifted files list by moving them in a direction
based on VECTOR, which is an integer (positive shifts toward end
of list, negative toward beginning).

Considers two cases (as of 2023-03-10):
1. Fewer files tracked than windows displayed
  - The function only allows shifting the list to and fro between
    the current number of windows. It barfs when the notes reach
    the end of the window list bounds.
2. Many files few(er) windows
  - The function shifts all the way to the end of the list, detects 
    that you're at the end (or beginning) and barfs."
  (let* ((wl (zk-desktop--get-window-list))
	 (fl (mapcar #'zk-desktop--window-buffer-file wl)))
    (cond ((> (length wl) (length zk-desktop--ids)) ;; More windows than files
	   (zk-desktop--shift-few-files wl fl vector))
	  (t (zk-desktop--shift-many-files wl fl vector)) ;; Plenty of files, fewer windows
	  )))

(defun zk-desktop--window-buffer-file (window)
  (when (not (equal (window-buffer window) (zk-desktop--blank-buffer)))
    (file-relative-name (buffer-file-name (window-buffer window))
			zk-desktop-zettelkasten-directory)))

(defun zk-desktop--redisplay-files (new-ids)
  "Redisplays files using the existing amount of windows."
  (let ((wl (zk-desktop--get-window-list))
	buffer)
    (dotimes (i (length wl))
      (let ((id-elt (elt new-ids i))
	    buffer)
	(when (or (bufferp id-elt)
		  (null id-elt))
	  (setq buffer (zk-desktop--blank-buffer)))
	(let ((buffer (or buffer (find-file-noselect (zk-desktop--zettel-truename-from-id id-elt)))))
	  (set-window-buffer (elt wl i) buffer))))))

(defun zk-desktop--shift-notes-sequence (increment)
  (let ((new-files (zk-desktop--make-shifted-files-list increment)))
    (zk-desktop--redisplay-files new-files)))

(defun zk-desktop-shift-notes-sequence-down ()
  "Shifts the files displayed toward the end of the list by one."
  (interactive)
  (zk-desktop--shift-notes-sequence 1))

(defun zk-desktop-shift-notes-sequence-up ()
  "Shifts the files displayed toward the beginning of the list by
one."
  (interactive)
  (zk-desktop--shift-notes-sequence -1))

(defvar zk-desktop--shifter
  (let ((map (make-sparse-keymap)))
    (dolist (mods '(() (control)))
      (dolist (key '(?- ?+ ?= ?u ?d ?n ?p)) ;; = is often unshifted +.
	(define-key map (vector (append mods (list key)))
		    #'(lambda () (interactive) (zk-desktop-shift-notes-sequence 1)))))
    map)
  "This is the shifter most recently used. The default value shifts by one note. This is used to restart a shifting temporary keymap.")

(defconst zk-desktop--default-shifting-instructions
  "Use +,-,d,u,C-d,C-u for more shifts"
  "Default message for instructing the user to shift up or down the
file list.")

(defvar zk-desktop--shifting-instructions
  zk-desktop--default-shifting-instructions
  "Message for instructing the user to shift up or down the file list.")

(defun zk-desktop-shift-notes-sequence (increment)
  "Shifts the files displayed either up or down, which is toward
the end or beginning respectively. Also sets up a transient
keymap to keep doing so.

Use numeric prefix argument INCREMENT to shift by that many
places."
  (interactive "p")
  (let ((ev last-command-event)
	(echo-keystrokes nil))
    (let* ((base (event-basic-type ev))
           (step
            (pcase base
              ((or ?+ ?= ?d ?n) increment)
              ((or ?- ?u ?p) (- increment))
              (_ increment))))
      (zk-desktop--shift-notes-sequence step)
      (message zk-desktop--shifting-instructions)
      (setq zk-desktop--shifting-instructions zk-desktop--default-shifting-instructions )
      (set-transient-map
       (let ((map (make-sparse-keymap)))
	 (dolist (mods '(() (control)))
	   (dolist (key '(?- ?+ ?= ?u ?d ?n ?p)) ;; = is often unshifted +.
	     (define-key map (vector (append mods (list key)))
			 #'(lambda () (interactive) (zk-desktop-shift-notes-sequence (abs increment))))))
	 (setq zk-desktop--shifter map)
	 )))))


;;;;; Utility functions

(defun zk-desktop--window-tree-no-minibuf ()
  (elt (window-tree) 0))

(defun zk-desktop--count-columns ()
  (let ((wt (zk-desktop--window-tree-no-minibuf)))
    (if (and (window-parent) (not (car wt)))
        (seq-let [_ _ &rest cols] wt
          (length cols))
      1)))

(defun zk-desktop--single-cols-only-rows-only-or-grid (wt)
  "Returns SINGLE, ROWS-ONLY, COLS-ONLY, or GRID based on arg WT,
  assumed to be a window-tree that leaves off the minibuffer (see
  `zk-desktop--window-tree-no-minibuf'). "
  (cond ((windowp wt) 'single)
        ((listp wt)
         (seq-let [direction _ &rest cols] wt
           (if (cl-every #'windowp cols)
               (if direction
                   'rows-only
                 'cols-only)
             'grid)))))

(defun zk-desktop--count-rows ()
  "Counts the rows in the first column. Assumes all columns are
  based on splits of the top left window."
  (let* ((wt (zk-desktop--window-tree-no-minibuf))
         (cls (zk-desktop--single-cols-only-rows-only-or-grid wt)))
    (cond ((or (eq cls 'single)
               (eq cls 'cols-only))
           1)
          ((eq cls 'rows-only)
           (seq-let [_ _ &rest rows] wt
             (length rows)))
          ((eq cls 'grid)
           (seq-let [_ _ &rest cols] wt
             (length (cddr (cl-first cols))))))))

(defun zk-desktop--blank-buffer ()
  (let ((blank (get-buffer-create "*BLANK*")))
    (with-current-buffer blank
      (unless buffer-read-only
	(read-only-mode 1)))
    blank))

(defun zk-desktop--get-window-list ()
  (window-list nil nil (frame-first-window)))

(defun zk-desktop--get-window-list-no-blanks ()
  (-remove #'(lambda (item)
	       (equal (zk-desktop--blank-buffer)
		      (window-buffer item)))
	   (zk-desktop--get-window-list)))

(defun zk-desktop--get-displayed-files ()
  (mapcar #'zk-desktop--window-buffer-file (zk-desktop--get-window-list)))

(defun zk-desktop--get-displayed-files-no-blanks ()
  (mapcar #'zk-desktop--window-buffer-file (zk-desktop--get-window-list-no-blanks)))

(defun zk-desktop--get-selected-window-index (&optional window)
  "Returns a zero-based index of WINDOW in the grid / window list.

If nil WINDOW defaults to `selected-window'."
  (if window
      (seq-position (zk-desktop--get-window-list)
		    window)
    (seq-position (zk-desktop--get-window-list)
		  (selected-window))))

(defun zk-desktop--get-window-index (&optional window)
  "Returns a zero-based index of WINDOW in the grid / window list.
Ignores *BLANK* buffers.

If nil WINDOW defaults to `selected-window'."
  (if window
      (seq-position (zk-desktop--get-window-list-no-blanks)
		    window)
    (seq-position (zk-desktop--get-window-list-no-blanks)
		  (selected-window))))

(defun zk-desktop--column-split-decider ()
  (let ((columns (zk-desktop--count-columns)))
    (cond ((equal columns 1) #'frame-root-window)
	  (t #'(lambda () (window-parent (elt (window-list nil nil (frame-first-window)) (* (1- columns) zk-desktop-max-rows))))))))



;;;###autoload
(defun zk-desktop-delete-column ()
  (interactive)
  (let ((column-count (zk-desktop--count-columns))
	(row-count (zk-desktop--count-rows)))
    (when (> column-count 1)
      (let* ((wl (zk-desktop--get-window-list))
	     (cursor-in-column-num (floor (zk-desktop--get-selected-window-index) row-count))
	     (start (* cursor-in-column-num row-count)))
	(dotimes (i row-count)
	  (delete-window (elt wl (+ i start))))
	(balance-windows)))))

;;;###autoload
(defun zk-desktop-delete-row ()
  (interactive)
  ;; (let ((cols (zk-desktop--count-columns))
  ;;       (rows (zk-desktop--count-rows)))
  ;;   (zk-desktop--update-grid (1- rows) cols))
  )


(defun zk-desktop--get-blank-buffer-window (&optional index)
  "Returns the first *BLANK* buffer window, or the INDEX *BLANK*
buffer window if provided."
  (unless index
    (setq index 0))
  (when-let ((blank (nth index (get-buffer-window-list (zk-desktop--blank-buffer)))))
    blank))

(defvar zk-desktop--opposites '(rows columns columns rows)
  "Useful plist for getting the opposite of the preference.")

(defun zk-desktop--first-check ()
  (cond ((equal zk-desktop-prefer-rows-or-columns 'rows)
	 (cons zk-desktop-max-rows (zk-desktop--count-rows)))
	((equal zk-desktop-prefer-rows-or-columns 'columns)
	 (cons zk-desktop-max-columns (zk-desktop--count-columns)))
	(t (user-error "%s" "`zk-desktop-prefer-rows-or-columns' is malformed."))))

(defun zk-desktop--second-check ()
  (cond ((equal (plist-get zk-desktop--opposites zk-desktop-prefer-rows-or-columns) 'rows)
	 (cons zk-desktop-max-rows (zk-desktop--count-rows)))
	((equal (plist-get zk-desktop--opposites zk-desktop-prefer-rows-or-columns) 'columns)
	 (cons zk-desktop-max-columns (zk-desktop--count-columns)))
	(t (user-error "%s" "`zk-desktop-prefer-rows-or-columns' is malformed."))))

(defun zk-desktop--decide-add-row-or-column ()
  (let* ((first-check (zk-desktop--first-check))
	 (second-check (zk-desktop--second-check)))
    (cond ((> (car first-check) (cdr first-check)) zk-desktop-prefer-rows-or-columns)
	  ((> (car second-check) (cdr second-check))
	   (plist-get zk-desktop--opposites zk-desktop-prefer-rows-or-columns)))))

;;;###autoload
(defun zk-desktop-blank-this-window ()
  "Makes the selected-window a *BLANK* buffer. You will no longer
be displaying the selected-window buffer if not displayed
elsewhere. "
  (interactive)
  (set-window-buffer (selected-window) (zk-desktop--blank-buffer)))

;;;; User Functions and Entry Points

;;;;;; Jump to zettel

(defvar zk-desktop--read-zettel-history nil
  "History variable for `zk-desktop-read-zettel'")

(defun zk-desktop-read-zettel (&optional zettels callback)
  (interactive)
  (let ((vertico-sort-function nil))
    (zk-desktop--alist-complete (or zettels (zk-desktop--current-zettels-indexed-alist))
				"Select a zettel: "
				(or callback #'cadr)
				'zk-desktop--read-zettel-history)))

(defun zk-desktop--read-zettel (&optional zettels callback)
  (interactive)
  (let ((vertico-sort-function nil))
    (zk-desktop--alist-complete (or zettels (zk-desktop--indexed-alist-for-zettel-read))
				"Which zettel? "
			        (or callback #'cadr)
				nil
				nil
			        t)))

(defun zk-desktop--indexed-alist-for-zettel-read ()
  (cl-loop for id in zk-desktop--ids
	   for i from 0
	   collect (list i id (zk-desktop--nice-note-selection id t))))

(defun zk-desktop-jump-to-zettel ()
  "Jumps to a `completing-read'-selected zettel by placing it at
the top left of your desktop grid of windows. Fills the remaining
windows in the current grid configuration."
  (interactive)
  (zk-desktop--redisplay-files (seq-take (seq-subseq zk-desktop--ids (zk-desktop-read-zettel nil #'car))
					 (length (zk-desktop--get-window-list)))))

;;;;;; Dired

;;;###autoload
(defun zk-desktop-dired-find-file ()
  "Finds the dired file, displaying it in using `zk-desktop'."
  (interactive)
  (let* ((filename (car (dired-get-marked-files)))
         (zettelp (file-exists-p (concat zk-desktop-zettelkasten-directory
					 (file-name-nondirectory filename))))
	 (file-buffer (if zettelp
			  (get-file-buffer filename)
			(user-error "Tried to display a non-zettel using zk-desktop"))))
    (unless file-buffer
      (setq file-buffer (find-file-noselect filename)))
    (zk-desktop--display-buffer file-buffer)))

;;;;;; Simple Completion

(defun zk-desktop-find-note ()
  "Finds a permanent note from your `zk-desktop-zettelkasten-directory' and displays it in the next logical window."
  (interactive)
  (let ((note (read-file-name "Select a note: " zk-desktop-zettelkasten-directory nil t)))
    (zk-desktop--display-buffer (find-file-noselect note))))

;;;;;; Denote

(require 'denote)

;; Needs bound in denote keymap by activation of the zk-desktop minor mode
;;;###autoload
(defun zk-desktop-denote-link-find-note ()
  "Use minibuffer completion to visit a denote linked file, as in
Prot's `denote' package (`denote-link-find-file'). However, this
is an altered version of the function that passes the file to
zk-desktop and finds it in a *BLANK* window buffer."
  (interactive)
  (if-let* ((current-file (buffer-file-name))
            (file-type (denote-filetype-heuristics current-file))
            (regexp (denote--link-in-context-regexp file-type))
            (files (denote-link--expand-identifiers regexp)))
      (zk-desktop--display-buffer (find-file-noselect (denote-link--find-file-prompt files)))
    (user-error "No links found in the current buffer")))

;;;###autoload
(defun zk-desktop-denote-backlink-find-note ()
  "Finds a note that is a backlink for the current note.

This function is a hacked version of `denote-link-backlinks'."
  (interactive)
  (if-let* ((file (buffer-file-name))
            (id (denote-retrieve-filename-identifier file))
            (files (delete file (denote--retrieve-files-in-xrefs id))))
      (zk-desktop--display-buffer (find-file-noselect (denote-link--find-file-prompt files)))
    (user-error "No links found in the current buffer")))


;;;; Package Keymap definition

(defvar zk-desktop-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "f") #'zk-desktop-find-note)
    (define-key map (kbd "C-f") #'zk-desktop-find-note)
    (define-key map (kbd "SPC") #'zk-desktop-blank-this-window)
    (define-key map (kbd "C-SPC") #'zk-desktop-blank-this-window)
    (define-key map (kbd "l") #'zk-desktop-denote-link-find-note)
    (define-key map (kbd "C-l") #'zk-desktop-denote-link-find-note)
    (define-key map (kbd "b") #'zk-desktop-denote-backlink-find-note)
    (define-key map (kbd "C-b") #'zk-desktop-denote-backlink-find-note)
    (define-key map (kbd "d c") #'zk-desktop-delete-column)
    (define-key map (kbd "d r") #'zk-desktop-delete-row)
    (define-key map (kbd "C-u") #'zk-desktop-shift-notes-sequence)
    (define-key map (kbd "C-d") #'zk-desktop-shift-notes-sequence)
    (define-key map (kbd "C-n") #'zk-desktop-shift-notes-sequence)
    (define-key map (kbd "C-p") #'zk-desktop-shift-notes-sequence)
    (define-key map (kbd "j") #'zk-desktop-jump-to-zettel)
    (define-key map (kbd "v") #'zk-desktop-view-more-notes)
    (define-key map (kbd "s") #'zk-desktop-save-current-file-list)
    (define-key map (kbd "m") #'zk-desktop-move-note)
    (define-key map (kbd "p") #'zk-desktop-place-note)
    (define-key map (kbd "M-s") #'zk-desktop-save-copy-of-file-list)
    map))

(defun zk-desktop-bind-dired-RET (&optional key)
  "Binds C-<return> (or other KEY combo, a string that can be used
by `kbd') to a function that uses `zk-desktop--display-buffer' as
the `display-buffer-overriding-action'.

Useful for configuring your emacs. Feel free to just configure
the same command yourself! This is a helper function. "
  (interactive)
  (let ((bind-key (if key key "C-<return>")))
    (with-eval-after-load 'dired
      (define-key dired-mode-map (kbd bind-key) #'zk-desktop-dired-find-file))))

(provide 'zk-desktop)

;;; zk-desktop.el ends here
