;;; zk-desktop-test.el --- test some things for zk-desktop   -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Alec Kornblum

;; Author: Alec M. Kornblum <alec.kornblum@gmail.com>
;; Version: 1.0
;; Package-Requires: ((emacs "28.1") (zk-desktop "0.1"))
;; Keywords: windows
;; URL: TBA

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; This file is not part of GNU Emacs

;;; Commentary:

;; This file tests zk-desktop package functionality.

;;; Code:

(require 'zk-desktop)

(defvar zk-desktop--tests nil
  "Collection of tests to run to test `zk-desktop'")

(defun zk-desktop--test-index-mover ()
  (interactive)
  (let ((parameters '((1 5) (5 1) (0 15) (5 -1) (5 5)))
	(mylist '(0 1 2 3 4 5 6 7 8 9))
	(successes '((0 2 3 4 5 1 6 7 8 9)
		     (0 5 1 2 3 4 6 7 8 9)
		     (1 2 3 4 5 6 7 8 9 0)
		     (5 0 1 2 3 4 6 7 8 9)
		     (0 1 2 3 4 5 6 7 8 9)))
	results)
    (dolist (indices parameters)
      (seq-let (move-index to-this-index) indices
	(push (zk-desktop--move-note mylist move-index to-this-index) results)))
    (print (reverse results))
    (print successes)
    (print (equal successes (reverse results)))))
(add-to-list 'zk-desktop--tests #'zk-desktop--test-index-mover)


(defun zk-desktop--test-find-subseq ()
  (interactive)
  (let ((sequences '((1 2 3 4 5 6 7 8 9 10 11 12)
		     ("one" "two" "three" "four" "five" "six" "seven" "eight")
		     (1 2 3 4 5 6 7 8 9 10 11 12)
		     ("one" "two" "three" "four" "five" "six" "seven" "eight")))
	(subseqs '((4 5 6 7 8 9)
		   ("three" "four" "five")
		   (1 3 4)
		   ("six" "seven" "three")))
	(successes '(3 2 nil nil))
	results)
    (setq results
	  (cl-loop for seq in sequences
		   for i from 0
		   collect (zk-desktop--find-subseq seq (elt subseqs i))))
    (print results)
    (print successes)
    (print (equal successes results))))
(add-to-list 'zk-desktop--tests #'zk-desktop--test-find-subseq)

(defun zk-desktop-run-tests ()
  (interactive)
  (dolist (test zk-desktop--tests)
    (call-interactively test)))

(provide 'zk-desktop-test)

;;; zk-desktop.el ends here
